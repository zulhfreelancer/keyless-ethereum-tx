## Keyless Transaction

A simple ExpressJS x EthersJS app that sends user input (number) to smart contract in Ethereum blockchain without requiring private key from user (server owner pays for it).

### Notes

Be sure to put private key (64 characters long) in the _private-key.txt_ file before running `node app.js` command. Otherwise, it won't work as expected.
